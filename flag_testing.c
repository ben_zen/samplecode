#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
  char short_form;
  char *long_form;
  char value_required;
} flag_t;

typedef struct flag_list_tag {
  flag_t flag;
  struct flag_list_tag *next;
} flag_list;

typedef struct {
  char *name;
  char *value;
} option_t;

typedef enum {
  Option,
  Parameter
} param_format;

typedef struct {
  param_format format;
  union {
    option_t option;
    char * parameter;
  };
} param_t;

typedef struct p_list_tag {
  param_t param;
  struct p_list_tag *next;
} param_list;

int parse_flag (char *line, flag_t *flag) {
  int rv = 1;
  unsigned long line_length = 0;
  char *flag_line = line;
  memset(flag, 0, sizeof(flag_t));
  if ((*flag_line) == '*') {
    flag->value_required = 1;
    flag_line++;
  }
  flag->short_form = *flag_line;
  if (*(++flag_line) == ':') {
    flag_line++;
    line_length = strlen(flag_line);
    // This code presupposes the presence of a '\n' on the string, consider fixing?
    flag->long_form = (char *) malloc(sizeof(char) * (line_length));
    if (flag->long_form != NULL) {
      memset(flag->long_form, 0, line_length);
      memcpy(flag->long_form, flag_line, line_length - 1);
      rv = 0;
    }
  }

  return rv;
}

int find_flag (char flag_char, flag_list *list_head, flag_t **flag) {
  int rv = 1;
  flag_list *iter = list_head;
  while (iter) {
    if (iter->flag.short_form == flag_char) {
      *flag = &(iter->flag);
      break;
    } else {
      iter = iter->next;
    }
  }
  return rv;
}

// Need a function to parse the option line and generate the list of invoked
// flags.

// Returns the number of parameters read. (Maybe?)
int parse_parameters (char *param_line, flag_list *flags, param_list **params_head) {
  int rv = 0;
  char *iter = param_line;
  param_list **next_param = params_head;
  while ((iter != NULL) && (*iter != '\n')) {
    // Skip whitespace
    if ((*iter == ' ') || (*iter == '\t')) {
      iter++;
      continue;
    } else if (*iter == '-') {
      // Either flag parsing or long option parsing.
      iter++;
      if (*iter == '-') {
	// Long parameter parsing.
	char *pname_start = NULL;
	char *pvalue_start = NULL;
	unsigned long pname_len = 0;
	unsigned long pvalue_len = 0;
	iter++;
        pname_start = iter;
	while ((*iter != ' ') && (*iter != '\t') && (*iter != '\n') &&
               (*iter != 0) && (*iter != '=')) {
	  iter++;
	  pname_len++;
	}
        if (*iter == '=') {
          pvalue_start = ++iter;
        }
        while ((*iter != ' ') && (*iter != '\t') && (*iter != '\n') &&
               (*iter != 0)) {
          iter++;
          pvalue_len++;
        }
        *next_param = (param_list *) malloc(sizeof(param_list));
        if (*next_param) {
          memset(*next_param, 0, sizeof(param_list));
          (*next_param)->param.format = Option;
          if (pname_len) {
            (*next_param)->param.option.name = (char *)
              malloc(sizeof(char) * (pname_len + 1));
          }
          if ((*next_param)->param.option.name) {
            memset((*next_param)->param.option.name, 0, pname_len + 1);
            memcpy((*next_param)->param.option.name, pname_start, pname_len);
          }
          // This code presupposes that a value is acceptable; because of the
          // problem definition, it needs to, but that's an open issue.
          if (pvalue_len) {
            (*next_param)->param.option.value = (char *)
              malloc(sizeof(char) * (pvalue_len + 1));
          }
          if ((*next_param)->param.option.value) {
            memset((*next_param)->param.option.value, 0, pvalue_len + 1);
            memcpy((*next_param)->param.option.value, pvalue_start, pvalue_len);
          }
          next_param = &((*next_param)->next);
        }
      } else {
	// Flag parsing.
	int continue_parsing = 1;
	while (continue_parsing) {
	  char p_short = 0;
	  flag_t *p_flag = NULL;
	  char *p_long = NULL;
	  char *p_value = NULL;
	  // Loop over the flags passed in a block; for each one, find its
	  // associated flag. If this parameter requires a value, follow a
	  // different parsing route.
	  p_short = *iter;
	  rv = find_flag(p_short, flags, &p_flag);
	  if (!rv) {
	    // Flag found!
	    p_long = p_flag->long_form;
	    if (p_flag->value_required) {
	      // In this case, there cannot be another flag immediately after this
	      // one.
	      continue_parsing = 0;
	      if (*(++iter) == ' ') {
		char *val_start = ++iter;
		unsigned long val_len = 0;
		while ((*iter != ' ') && (*iter != '\t') && (*iter != '\n') && (*iter != 0)) {
		  iter++;
		  val_len++;
		}
		if (val_len) {
		  p_value = (char *) malloc(sizeof(char) * (val_len + 1));
		  if (p_value) {
		    memset(p_value, 0, val_len + 1);
		    memcpy(p_value, val_start, val_len);
		  } else {
		    rv = 1;
		  }
		}
	      } else {
		rv = 1;
	      }
	    }
	    *next_param = (param_list *) malloc(sizeof(param_list));
	    if (*next_param) {
	      memset(*next_param, 0, sizeof(param_list));
	      (*next_param)->param.format = Option;
	      (*next_param)->param.option.name = p_long;
	      (*next_param)->param.option.value = p_value;
	      next_param = &((*next_param)->next);
	    } else {
	      continue_parsing = 0;
	    }
	  }
	  if (rv) {
	    break;
	  }
	}
      }
    } else {
      // Generic parameter.
      char *param_start = iter;
      char *param_text = NULL;
      unsigned long param_len = 0;
      while ((*iter != ' ') && (*iter != '\t') && (*iter != '\n') && (*iter != 0)) {
	iter++;
	param_len++;
      }
      // Allocate a string for the parameter.
      param_text = (char *) malloc(sizeof(char) * (param_len + 1));
      memset(param_text, 0, (param_len + 1));
      memcpy(param_text, param_start, param_len);
      // Allocate and fill the param_list item for this parameter.
      *next_param = (param_list *) malloc(sizeof(param_list));
      if (*next_param) {
	memset(*next_param, 0, sizeof(param_list));
	(*next_param)->param.format = Parameter;
	(*next_param)->param.parameter = param_text;
	next_param = &((*next_param)->next);
      }
    }
  }
  // Consider cleanup if `rv` is non-zero.
  return rv;
}

int print_parameters(param_list *parameters) {
  param_list *next_param = parameters;
  while (next_param != NULL) {
    switch (next_param->param.format) {
    case Option:
      if (next_param->param.option.value) {
        printf("flag: %s (value: %s)\n", next_param->param.option.name,
               next_param->param.option.value);
      } else {
        printf("flag: %s\n", next_param->param.option.name);
      }
      break;
    case Parameter:
      printf("parameter: %s\n", next_param->param.parameter);
      break;
    default:
      break;
    }
    next_param = next_param->next;
  }
  return 0;
}

int main(int argc, char **argv) {
  int rv = 0;
  long flag_count = 0;
  char input[256] = {0};
  flag_list *flags = NULL;
  param_list *parameters = NULL;
  memset(input, 0, 256);
  // Get number of flags
  fgets(input, 255, stdin);
  puts("Number of flags:\n");
  fflush(stdout);
  flag_count = strtol(input, NULL, 10);
  if (flag_count) {
    long iter = 0;
    flag_list *flag_iter = NULL;
    puts("Input flags:\n");
    fflush(stdout);
    flags = (flag_list *) malloc(sizeof(flag_list));
    if (flags) {
      flag_iter = flags;
      while(iter < flag_count) {
	memset(flag_iter, 0, sizeof(flag_list));
	memset(input, 0, 256);
	fgets(input, 255, stdin);
	rv = parse_flag(input, &(flag_iter->flag));
	if (!rv) {
	  if (++iter < flag_count) {
	    flag_iter->next = (flag_list *) malloc(sizeof(flag_list));
	    if (flag_iter->next) {
	      flag_iter = flag_iter->next;
	    } else {
	      rv = 1;
	      break;
	    }
	  }
	} else {
	  iter++; // Skip a failed flag parsing and continue on anyways.
	}
      }
    } else {
      rv = 1;
    }
  }
  memset(input, 0, 256);
  fgets(input, 255, stdin);
  rv = parse_parameters(input, flags, &parameters);

  if (!rv) {
    print_parameters(parameters);
  }

  return rv;
}
